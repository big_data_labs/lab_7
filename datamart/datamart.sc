import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.ClusteringEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.ml.feature.{StandardScaler, VectorAssembler}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.DataFrame


trait Database {
  def getData(): DataFrame
  def setPredictions(df: DataFrame): Unit
}

class SparkConfig(
    val appName: String,
    val deployMode: String,
    val driverMemory: String,
    val executorMemory: String,
    val executorCores: Int,
    val driverCores: Int
)

class DbConfig(
    val url: String,
    val user: String,
    val password: String,
    val driver: String
)

class SQLServer(config: SparkConfig, val dbConfig: DbConfig) extends Database {
    private val spark = SparkSession.builder()
        .appName(config.appName)
        .master(config.deployMode)
        .config("spark.driver.cores", config.driverCores)
        .config("spark.executor.cores", config.executorCores)
        .config("spark.driver.memory", config.driverMemory)
        .config("spark.executor.memory", config.executorMemory)
        .config("spark.jars", "jars/mssql-jdbc-12.6.1.jre11.jar")
        .config("spark.driver.extraClassPath", "jars/mssql-jdbc-12.6.1.jre11.jar")
        .getOrCreate()

    override def getData(): DataFrame = {
        val jdbcOptions = Map(
        "url" -> dbConfig.url,
        "dbtable" -> "FoodProducts",
        "user" -> dbConfig.user,
        "password" -> dbConfig.password,
        "driver" -> dbConfig.driver
        )

        spark.read.format("jdbc").options(jdbcOptions).option("inferSchema", "true").load()
    }

    override def setPredictions(df: DataFrame): Unit = {
        val jdbcOptions = Map(
        "url" -> dbConfig.url,
        "dbtable" -> "FoodProducts",
        "user" -> dbConfig.user,
        "password" -> dbConfig.password,
        "driver" -> dbConfig.driver
        )

        df.write.mode("append").format("jdbc").options(jdbcOptions).save()
    }
}

class DataMart(sparkConfig: SparkConfig, dbConfig: DbConfig) {
    private val FEATURES_COLUMN = "scaled_features"
    private val COLUMNS = Map(
        "id" -> Array("code", "product_name"),
        "numeric" -> Array(
        "energy_kcal_100g",
        "energy_100g",
        "fat_100g",
        "saturated_fat_100g",
        "trans_fat_100g",
        "cholesterol_100g",
        "carbohydrates_100g",
        "sugars_100g",
        "fiber_100g",
        "proteins_100g",
        "salt_100g",
        "sodium_100g",
        "calcium_100g",
        "iron_100g",
        "nutrition_score_fr_100g"
        ),
        "categorical" -> Array("categories_en")
    )
    private val database: Database = new SQLServer(sparkConfig, dbConfig)

    private def preprocess(df: DataFrame): DataFrame = {
        val idColumns = COLUMNS.apply("id")
        val featureColumnNames = COLUMNS.apply("numeric")
        val featureColumns = featureColumnNames.map(c => col(c).cast("float"))
        val catColumns = COLUMNS.apply("categorical")

        val allColumns = idColumns.map(col) ++ featureColumns ++ catColumns.map(col)
        val dfWithSelectedColumns = df.select(allColumns: _*)

        val vac_assembler = new VectorAssembler().setInputCols(featureColumnNames).setOutputCol("features")
        val dfWithFeatures = vac_assembler.transform(dfWithSelectedColumns)

        val scaler = new StandardScaler().setInputCol("features").setOutputCol(FEATURES_COLUMN)
        val scalerModel = scaler.fit(dfWithFeatures)
        scalerModel.transform(dfWithFeatures)
    }

    def getFood(): DataFrame = {
        val data = database.getData()
        preprocess(data)
    }

    def setPredictions(df: DataFrame): Unit = {
        database.setPredictions(df.select("code", "prediction"))
    }
}


val sparkConfig = new SparkConfig(
  appName = "testing",
  deployMode = "local",
  driverMemory = "2g",
  executorMemory = "2g",
  executorCores = 2,
  driverCores = 2
)

val dbConfig = new DbConfig(
  url = "mongo_dsn=mongodb://localhost:27017/",
  user = "",
  password = "",
  driver = "com.mongodb.spark.sql.DefaultSource"
)


val dataMart = new DataMart(sparkConfig, dbConfig)
