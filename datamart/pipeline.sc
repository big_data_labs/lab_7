import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.ClusteringEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


class SparkConfig(
    val appName: String,
    val deployMode: String,
    val driverMemory: String,
    val executorMemory: String,
    val executorCores: Int,
    val driverCores: Int
)


val spark = SparkSession.builder.appName("testing").config("spark.master", "local").getOrCreate()

val data = spark.read.option("header", "true").option("inferSchema", "true").csv("./data/dataset.csv")

data.printSchema()

val assembler = new VectorAssembler().setInputCols(Array()).setOutputCol("features")

val featureData = assembler.transform(data)

val kmeans = new KMeans().setK(3).setSeed(1L)
val model = kmeans.fit(featureData)

val predictions = model.transform(featureData)


val evaluator = new ClusteringEvaluator()

val silhouette = evaluator.evaluate(predictions)
println(s"Silhouette with squared euclidean distance = $silhouette")


println("Cluster Centers: ")
model.clusterCenters.foreach(println)

spark.stop()
