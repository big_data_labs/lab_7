import argparse

from loguru import logger

from spark import config
from spark.pipelines.basic import basic_pipeline
from spark.pipelines.word_count import word_count_pipeline


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Spark run entrypoint')
    parser.add_argument(
        'pipeline',
        choices=['basic', 'word_count'],
        help='The pipeline to run'
    )

    args = parser.parse_args()

    match args.pipeline:
        case 'basic':
            logger.info("Start basic pipeline")
            basic_pipeline()
        case 'word_count':
            logger.info("Start word_count pipeline")
            word_count_pipeline(config.DATA_DIR / "input.txt")
