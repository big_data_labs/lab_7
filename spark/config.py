import configparser
from functools import lru_cache
from pathlib import Path

from pydantic import Field, MongoDsn
from pydantic_settings import BaseSettings, SettingsConfigDict

ROOD_DIR = Path(__file__).parent.parent
DATA_DIR = ROOD_DIR / "data"

CONFIG = configparser.ConfigParser()
CONFIG.read(ROOD_DIR / 'config.ini')


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_nested_delimiter='__')

    mongo_dsn: MongoDsn = Field('mongodb://localhost:27017/', title="MongoDB dsn")


@lru_cache(maxsize=1)
def get_settings() -> Settings:
    return Settings()
