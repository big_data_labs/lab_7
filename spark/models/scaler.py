import configparser

from loguru import logger
from pyspark.ml.feature import StandardScaler
from pyspark.sql import DataFrame

from spark.config import CONFIG


class Scaler:
    def __init__(self):
        input_col = CONFIG['vectorizer']['vectorizedColumnName']
        self.output_col = CONFIG['scaler']['scaledColumnName']

        self.scaler = StandardScaler(
            inputCol=input_col,
            outputCol=self.output_col,
            withStd=True,
            withMean=False,
        )

    def scale(self, dataset: DataFrame):
        scaler_model = self.scaler.fit(dataset)
        scaled_data = scaler_model.transform(dataset)

        scaled_data.select(self.output_col).show(5)
        logger.info("Scaled data")
        return scaled_data
