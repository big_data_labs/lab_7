import configparser

from loguru import logger
from pyspark.ml.feature import VectorAssembler
from pyspark.sql import DataFrame

from spark.config import CONFIG


class Vectorizer:
    def __init__(self):
        self.output_col = CONFIG['vectorizer']['vectorizedColumnName']
        col_names = CONFIG['dataset']['featureColumns']
        self.input_cols = col_names.split(',')

        logger.info('input columns for vectorization: ', self.input_cols)

        self.vector_assembler = VectorAssembler(
            inputCols=self.input_cols,
            outputCol=self.output_col,
            handleInvalid='skip',
        )

    def vectorize(self, dataset: DataFrame):
        assembled_data = self.vector_assembler.transform(dataset)

        logger.info('Assembled data count: ', assembled_data.count())
        show_n = 5
        logger.info(f'First {show_n} vectorized: ')
        assembled_data.select(self.output_col).show(show_n)

        return assembled_data
