from pymongo import MongoClient

from spark.config import get_settings

settings = get_settings()


client = MongoClient(str(settings.mongo_dsn))
db = client['database_name']
collection = db['collection_name']