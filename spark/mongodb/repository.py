from loguru import logger
from pydantic import BaseModel
from pyspark.sql import SparkSession

from spark.config import get_settings
from spark.mongodb.client import collection

settings = get_settings()


def insert_data(results: list[BaseModel]) -> None:
    collection.insert_many([r.dict() for r in results])
    logger.info("Successfully insert in db")
